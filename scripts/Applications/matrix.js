var Matrix = function(){

    graphicsElement = os.resschmgr.Create.HTMLElement("div");
    graphicsElement.SetID('image-3d');
    
    graphicsDiv = document.getElementById('image-3d-window');
    
    thumbnailDiv = document.getElementById('thumbnail-window');
    optionsDiv = document.getElementById('properties-window');
    outputDiv = document.getElementById('output');
    
    bioCanvas = os.resschmgr.Create.HTMLElement("canvas");
    bioCanvas.html().style.width = "100%";
    bioCanvas.html().style.height = "100%";

    graphicsDiv.appendChild(bioCanvas.html());
    
    os.graphics.Load(false, false, bioCanvas.html());
    os.graphics.Managers.Texture.Create.Texture("starhawk", "scripts/jahova/OS/Cores/Graphics/textures/starhawk.jpg");
    os.graphics.Managers.Mesh.Create.Mesh("starhawk", "scripts/jahova/OS/Cores/Graphics/meshes/starhawk.json");
    
    
    
    gl = os.graphics.gl;
    
    dropDown = document.getElementById("dropdown");
    
    dropDown.onchange = function(e){
        win = os.windows.WindowsManager.Windows.get("jahova.window.id." + e.currentTarget.value);
        win.MakeActive();
    }
    
    
    
    //Setup Input Controls
    Input = {
        Mouse: {
            lastX: 0,
            lastY: 0,
            pressed: false
        }
    }
    onKeyDown = function(e){
        if(String.fromCharCode(e.keyCode) == "W"){     //Forward
            //os.graphics.Managers.Camera.MoveForward(10);
            sceneNode.Move.Forward(10);
        }
        else if(String.fromCharCode(e.keyCode) == "S"){     //Backware
            //os.graphics.Managers.Camera.MoveBackward(10);
            sceneNode.Move.Backward(10);
        }
        else if(String.fromCharCode(e.keyCode) == "A"){     //Straif Left
            //os.graphics.Managers.Camera.MoveLeft(10);
            sceneNode.Move.Left(10);
        }
        else if(String.fromCharCode(e.keyCode) == "D"){     //Straif Right
            //os.graphics.Managers.Camera.MoveRight(10);
            sceneNode.Move.Right(10);
        }
        else if(String.fromCharCode(e.keyCode) == "Q"){     //Straif Up
            //os.graphics.Managers.Camera.MoveUp(5);
            sceneNode.Move.Up(10);
        }
        else if(String.fromCharCode(e.keyCode) == "E"){     //Straif Down
            //os.graphics.Managers.Camera.MoveDown(10);
            sceneNode.Move.Down(10);
        }
        else if(String.fromCharCode(e.keyCode) == "F"){     //Straif Down
            fullscreen()
        }
        else if(e.keyCode == 27){     //Straif Down
            windowed();
        }
        mat4.identity(starhawk.Graphics.Matrix.Parent.Translate);
        mat4.translate(starhawk.Graphics.Matrix.Parent.Translate, sceneNode.Position);
        mat4.multiply(starhawk.Graphics.Matrix.Parent.Translate, starhawk.Graphics.Matrix.Parent.Rotation, starhawk.Graphics.Matrix.Parent.World );
    }
    onMouseDown = function(e){
        Input.Mouse.lastX = e.clientX;
        Input.Mouse.lastY = e.clientY;
        Input.Mouse.pressed = true;
    }
    
    onMouseUp = function(e){
        Input.Mouse.pressed = false;
    }
    
    onMouseMove = function(e){
        //if (!Input.Mouse.pressed) {
            return;
        //}
        //var cam = os.graphics.Managers.Camera;
        
        var newX = e.clientX;
        var newY = e.clientY;
    
        var deltaX = newX - Input.Mouse.lastX
        starhawk.yaw -= deltaX / 10;
        //cam.Rotation.yaw += deltaX / 10;
        
        //if(cam.Rotation.yaw > 360){ cam.Rotation.yaw  -= 360;}
        //else if(cam.Rotation.yaw < 0) { cam.Rotation.yaw += 360; }
        if(starhawk.yaw > 360){ starhawk.yaw  -= 360;}
        else if(starhawk.yaw < 0) { starhawk.yaw += 360; }
        
    
        var deltaY = newY - Input.Mouse.lastY;
        //cam.Rotation.pitch += deltaY / 10;
        starhawk.Default.pitch -= deltaY / 10;
        
        //if(cam.Rotation.pitch > 360){ cam.Rotation.pitch  -= 360;}
        //else if(cam.Rotation.pitch < 0) { cam.Rotation.pitch += 360; }
        
        if(starhawk.Default.pitch > 360){ starhawk.Default.pitch  -= 360;}
        else if(starhawk.Default.pitch < 0) { starhawk.Default.pitch += 360; }
    
        //Reset offset
        mat4.identity(starhawk.Graphics.Matrix.Parent.Rotation);
        
        //Rotate Scene Node
        mat4.rotateX(starhawk.Graphics.Matrix.Parent.Rotation, degToRad(starhawk.Default.pitch), starhawk.Graphics.Matrix.Parent.Rotation);
        
        //Update Scene World Matrix
        mat4.multiply(starhawk.Graphics.Matrix.Parent.Translate, starhawk.Graphics.Matrix.Parent.Rotation, starhawk.Graphics.Matrix.Parent.World );
        
        Input.Mouse.lastX = newX
        Input.Mouse.lastY = newY;   
    }
    
    
    // Setup Event Handlers for User Input
    window.addEventListener("keydown", onKeyDown, false);
    os.graphics.Get.Canvas().addEventListener("mousedown", onMouseDown, false);
    document.addEventListener("mouseup", onMouseUp, false);
    document.addEventListener("mousemove", onMouseMove, false);
    
    sceneNode = os.graphics.Managers.Entity.Create();
    //Load Graphics Object
    
    vec3.set([0,40,-130],os.graphics.Managers.Camera.Position);
    
    starhawk = os.graphics.Managers.Entity.Create();
    starhawk.Graphics.Add.Mesh("starhawk");
    starhawk.Graphics.Add.Texture("starhawk");
    starhawk.Scale = {
        x: 0.25,
        y: 0.25,
        z: 0.25
    }
    starhawk.Graphics.Vectors.Scale.set([starhawk.Scale.x, starhawk.Scale.y, starhawk.Scale.z]);
    starhawk.Move.Forward(100);
    starhawk.yaw = 135;
    starhawk.Default.pitch = 0;
    scX = 1;
    scY = 1;
    scZ = 1;
    os.graphics.AddToWorld(starhawk);
    
    
    BuildLightingWindow();
    BuildObjectWindow();
    BuildInstructionsWindow();
    
    os.graphics.Start();
    os.debugbar.Disable();
}

fullscreen = function(){
    
    //Scroll Screen to top left, to hide any overflow
    scrollTo(0,0);
    
    var tn = document.getElementById("thumbnail");
    tn.style.display = "none";
    
    var properties = document.getElementById("properties");
    properties.style.display = "none";
    
    var button = document.getElementById("expand-button");
    button.style.display = "none";
    
    var win = document.getElementById("image-3d-window");
    win.setAttribute("class", "fullscreen");
    win.style.height = window.innerHeight + "px";
    
    document.body.style.overflow = "hidden";
    
    //Resets the graphics projection matrix once animation of screen resize has finished
    setTimeout(os.graphics.OnReset, 500);
    
    //Hide Debug Bar, to make sure its not causing any problems
    os.debugbar.Disable();
}

windowed = function(){
    var tn = document.getElementById("thumbnail");
    tn.style.display = "";
    
    var properties = document.getElementById("properties");
    properties.style.display = "";
    
    var button = document.getElementById("expand-button");
    button.style.display = "";
    
    var win = document.getElementById("image-3d-window");
    win.setAttribute("class", "image-3d-window");
    win.style.height = "";
    
    document.body.style.overflow = "";
    
    //Resets the graphics projection matrix once animation of screen resize has finished
    setTimeout(os.graphics.OnReset, 500);
    
    //Hide Debug Bar, to make sure its not causing any problems
    os.debugbar.Enable();    
}

BuildObjectWindow = function(){
    //Set Up Control Window
    Win = os.windows.WindowsManager.Create.Window("Object Properties", "PC");
    //Win.elements.content.html().style.overflow = "hidden";
    Win.Set.statusbarText("");
    Win.Display.window();
    Win.elements.titlebar.buttons.close.html().onclick = function(e){
        DockWindow(os.windows.WindowsManager.Windows.get(e.target.id));    
    }
    
    
    //  X
    var scaleXLabel = os.resschmgr.Create.HTMLElement("label");
    scaleXLabel.html().for = "scalex";
    scaleXLabel.html().innerHTML = "<br/>Scale<br/>Scale&nbspX&nbsp&nbsp&nbsp";
    scaleXLabel.AppendTo(Win.elements.content.html());
    var scaleX = os.resschmgr.Create.HTMLElement("input");
    scaleX.html().type = "range";
    scaleX.html().id = "scalex";
    scaleX.html().min = 0.1;
    scaleX.html().max = 10;
    scaleX.html().step = 0.1;
    scaleX.html().value = 1;
    scaleX.html().onchange = function(e){
        scX = e.target.value;
        var multi = 1;
        if(mirrorCheckbox.html().checked){
            multi = -1;
        }

        starhawk.Set.Scale(multi * scX *starhawk.Scale.x, scY * starhawk.Scale.y,scZ *  starhawk.Scale.z);
        UpdateInstructionWindow();
    }
    scaleX.AppendTo(Win.elements.content.html());
    
    //  Y
    var scaleYLabel = os.resschmgr.Create.HTMLElement("label");
    scaleYLabel.html().for = "scaley";
    scaleYLabel.html().innerHTML = "<br/>Scale&nbspY&nbsp&nbsp&nbsp";
    scaleYLabel.AppendTo(Win.elements.content.html());
    var scaleY = os.resschmgr.Create.HTMLElement("input");
    scaleY.html().type = "range";
    scaleY.html().id = "scaley";
    scaleY.html().min = 0.1;
    scaleY.html().max = 10;
    scaleY.html().step = 0.1;
    scaleY.html().value = 1;
    scaleY.html().onchange = function(e){
        scY = e.target.value;
        var multi = 1;
        if(mirrorCheckbox.html().checked){
            multi = -1;
        }

        starhawk.Set.Scale(multi * scX *starhawk.Scale.x, scY * starhawk.Scale.y,scZ *  starhawk.Scale.z);
        UpdateInstructionWindow();
    }
    scaleY.AppendTo(Win.elements.content.html());
    
    //  Z
    var scaleZLabel = os.resschmgr.Create.HTMLElement("label");
    scaleZLabel.html().for = "scalez";
    scaleZLabel.html().innerHTML = "<br/>Scale&nbspZ&nbsp&nbsp&nbsp";
    scaleZLabel.AppendTo(Win.elements.content.html());
    var scaleZ = os.resschmgr.Create.HTMLElement("input");
    scaleZ.html().type = "range";
    scaleZ.html().id = "scalez";
    scaleZ.html().min = 0.1;
    scaleZ.html().max = 10;
    scaleZ.html().step = 0.1;
    scaleZ.html().value = 1;
    scaleZ.html().onchange = function(e){
        scZ= e.target.value;
        var multi = 1;
        if(mirrorCheckbox.html().checked){
            multi = -1;
        }

        starhawk.Set.Scale(multi * scX *starhawk.Scale.x, scY * starhawk.Scale.y,scZ *  starhawk.Scale.z);
        UpdateInstructionWindow();
    }
    scaleZ.AppendTo(Win.elements.content.html());
    
    
    
   
    //
    //  Rotate
    //
    //  Yaw Y
    var yawLabel = os.resschmgr.Create.HTMLElement("label");
    yawLabel.html().for = "yaw";
    yawLabel.html().innerHTML = "<br/><br/>Rotate<br/>Yaw&nbsp(Y)&nbsp&nbsp&nbsp";
    yawLabel.AppendTo(Win.elements.content.html());
    var yaw = os.resschmgr.Create.HTMLElement("input");
    yaw.html().type = "range";
    yaw.html().id = "yaw";
    yaw.html().min = 0.0;
    yaw.html().max = 360;
    yaw.html().step = 1.0;
    yaw.html().value = 135;
    yaw.html().onchange = function(e){
        starhawk.yaw = e.target.value;
        UpdateInstructionWindow();
    }
    yaw.AppendTo(Win.elements.content.html());
    
    //  Pitch X
    var pitchLabel = os.resschmgr.Create.HTMLElement("label");
    pitchLabel.html().for = "pitch";
    pitchLabel.html().innerHTML = "<br/>Pitch&nbsp(X)&nbsp&nbsp&nbsp";
    pitchLabel.AppendTo(Win.elements.content.html());
    var pitch = os.resschmgr.Create.HTMLElement("input");
    pitch.html().type = "range";
    pitch.html().id = "pitch";
    pitch.html().min = 0.0;
    pitch.html().max = 360;
    pitch.html().step = 1.0;
    pitch.html().value = 0;
    pitch.html().onchange = function(e){
        starhawk.pitch = e.target.value;
        UpdateInstructionWindow();
    }
    pitch.AppendTo(Win.elements.content.html());
    
    //  Roll Z
    var rollLabel = os.resschmgr.Create.HTMLElement("label");
    rollLabel.html().for = "roll";
    rollLabel.html().innerHTML = "<br/>Roll&nbsp(Z)&nbsp&nbsp&nbsp";
    rollLabel.AppendTo(Win.elements.content.html());
    var roll = os.resschmgr.Create.HTMLElement("input");
    roll.html().type = "range";
    roll.html().id = "roll";
    roll.html().min = 0.0;
    roll.html().max = 360;
    roll.html().step = 1.0;
    roll.html().value = 0;
    roll.html().onchange = function(e){
        starhawk.roll = e.target.value;
        UpdateInstructionWindow();
    }
    roll.AppendTo(Win.elements.content.html());
    
    //
    //  Translate
    //
    //  X
    var translateXLabel = os.resschmgr.Create.HTMLElement("label");
    translateXLabel.html().for = "translateX";
    translateXLabel.html().innerHTML = "<br/><br/>Translate<br/><bt/>X&nbsp&nbsp&nbsp&nbsp";
    translateXLabel.AppendTo(Win.elements.content.html());
    var translateX = os.resschmgr.Create.HTMLElement("input");
    translateX.html().type = "range";
    translateX.html().id = "translateX";
    translateX.html().min = 0.0;
    translateX.html().max = 200;
    translateX.html().step = 1.0;
    translateX.html().value = 0;
    translateX.html().onchange = function(e){
        starhawk.Position[0] = e.target.value;
        UpdateInstructionWindow();
    }
    translateX.AppendTo(Win.elements.content.html());
    
    //  Y
    var translateYLabel = os.resschmgr.Create.HTMLElement("label");
    translateYLabel.html().for = "translateY";
    translateYLabel.html().innerHTML = "<br/>Y&nbsp&nbsp&nbsp&nbsp";
    translateYLabel.AppendTo(Win.elements.content.html());
    var translateY = os.resschmgr.Create.HTMLElement("input");
    translateY.html().type = "range";
    translateY.html().id = "translateY";
    translateY.html().min = 0.0;
    translateY.html().max = 200;
    translateY.html().step = 1.0;
    translateY.html().value = 0;
    translateY.html().onchange = function(e){
        starhawk.Position[1] = e.target.value;
        UpdateInstructionWindow();
    }
    translateY.AppendTo(Win.elements.content.html());
    
    //   Z
    var translateZLabel = os.resschmgr.Create.HTMLElement("label");
    translateZLabel.html().for = "translateZ";
    translateZLabel.html().innerHTML = "<br/>Z&nbsp&nbsp&nbsp&nbsp";
    translateZLabel.AppendTo(Win.elements.content.html());
    var translateZ = os.resschmgr.Create.HTMLElement("input");
    translateZ.html().type = "range";
    translateZ.html().id = "translateZ";
    translateZ.html().min = 0.0;
    translateZ.html().max = 100;
    translateZ.html().step = 1.0;
    translateZ.html().value = 200;
    translateZ.html().onchange = function(e){
        starhawk.Position[2] = e.target.value;
        UpdateInstructionWindow();
    }
    translateZ.AppendTo(Win.elements.content.html());
    
    
    
    
     //Mirror Texture
    mirrorLabel = os.resschmgr.Create.HTMLElement("label");
    mirrorLabel.html().for = "mirror";
    mirrorLabel.html().innerHTML = "<br/><br/>Mirror Texture &nbsp &nbsp";
    mirrorLabel.AppendTo(Win.elements.content.html());
    
    mirrorCheckbox = os.resschmgr.Create.HTMLElement("input");
    mirrorCheckbox.html().id = "mirror";
    mirrorCheckbox.html().type = "checkbox";
    mirrorCheckbox.html().checked = true;
    mirrorCheckbox.AppendTo(Win.elements.content.html());
    
    mirrorCheckbox.html().onchange = function(e){
        
        var multi = 1;
        if(e.target.checked){multi = -1;}
        starhawk.Set.Scale(multi * scX *starhawk.Scale.x, scY * starhawk.Scale.y,scZ *  starhawk.Scale.z);
        
    }
    
    
    //Alpha Blend
    alphaLabel = os.resschmgr.Create.HTMLElement("label");
    alphaLabel.html().for = "alphacheckbox";
    alphaLabel.html().innerHTML = "<br/>Enable Alpha Scale &nbsp &nbsp";
    alphaLabel.AppendTo(Win.elements.content.html());
    
    alphaCheckbox = os.resschmgr.Create.HTMLElement("input");
    alphaCheckbox.html().id = "alphacheckbox";
    alphaCheckbox.html().type = "checkbox";
    alphaCheckbox.html().checked = false;
    alphaCheckbox.html().onchange = function(e){
        if(e.target.checked){
            starhawk.Graphics.Set.enableAlpha(true);
        }
        else{
            starhawk.Graphics.Set.enableAlpha(false);
        }
    }
    alphaCheckbox.AppendTo(Win.elements.content.html());
    
    
    var alphaScaleLabel = os.resschmgr.Create.HTMLElement("label");
    alphaScaleLabel.html().for = "alpha";
    alphaScaleLabel.html().innerHTML = "<br/>Alpha Value&nbsp&nbsp&nbsp&nbsp";
    alphaScaleLabel.AppendTo(Win.elements.content.html());
    var alpha = os.resschmgr.Create.HTMLElement("input");
    alpha.html().type = "range";
    alpha.html().id = "scale";
    alpha.html().min = 0.0;
    alpha.html().max = 1.0;
    alpha.html().step = 0.01;
    alpha.html().value = 0.5;
    alpha.html().onchange = function(e){
        if(alphaCheckbox.html().checked){
            starhawk.Graphics.Set.alphaValue(e.target.value);
        }
    }
    alpha.AppendTo(Win.elements.content.html());
    
    DockWindow(Win);
     
}

BuildLightingWindow = function(){
     Win2 = os.windows.WindowsManager.Create.Window("Lighting Properties", "PC");
    Win2.elements.content.html().style.overflow = "hidden";
    //Win2.elements.content.html().innerHTML= "Options Window 2";
    Win2.Set.statusbarText("");
    Win2.Display.window();
    Win2.elements.titlebar.buttons.close.html().onclick = function(e){
        DockWindow(os.windows.WindowsManager.Windows.get(e.target.id));    
    }
    //Add Lighting GUI controls

    //Ambinet Light Control
    var ambientLabel = os.resschmgr.Create.HTMLElement("label");
    ambientLabel.html().for = "ambientLight";
    ambientLabel.html().innerHTML = "Ambient Light&nbsp&nbsp&nbsp&nbsp";
    ambientLabel.AppendTo(Win2.elements.content.html());
    var ambientLightSlider = os.resschmgr.Create.HTMLElement("input");
    ambientLightSlider.html().type = "range";
    ambientLightSlider.html().id = "ambientLight";
    ambientLightSlider.html().min = 0;
    ambientLightSlider.html().max = 1;
    ambientLightSlider.html().step = 0.1;
    ambientLightSlider.html().value = 0.5;
    ambientLightSlider.html().onchange = function(e){
        var light = e.target.value;
        cancer.Graphics.Set.ambientColor([light, light, light]);
    }
    ambientLightSlider.AppendTo(Win2.elements.content.html());
    
    //Specular Highlight Control
    var specularEnableLabel = os.resschmgr.Create.HTMLElement("label");
    specularEnableLabel.html().for = "specularLightEnable";
    specularEnableLabel.html().innerHTML = "<br/>Enable Specular Highlights &nbsp &nbsp";
    specularEnableLabel.AppendTo(Win2.elements.content.html());
    
    specularLightEnableCheckbox = os.resschmgr.Create.HTMLElement("input");
    specularLightEnableCheckbox.html().id = "specularLightEnable";
    specularLightEnableCheckbox.html().type = "checkbox";
    specularLightEnableCheckbox.html().checked = false;
    specularLightEnableCheckbox.html().onchange = function(e){
        cancer.Graphics.Set.specular(e.target.checked);
    }
    specularLightEnableCheckbox.AppendTo(Win2.elements.content.html());
    
    var shininessLabel = os.resschmgr.Create.HTMLElement("label");
    shininessLabel.html().for = "shininess";
    shininessLabel.html().innerHTML = "<br/>Material Shininess&nbsp&nbsp";
    shininessLabel.AppendTo(Win2.elements.content.html());
    var shininessSlider = os.resschmgr.Create.HTMLElement("input");
    shininessSlider.html().type = "range";
    shininessSlider.html().id = "shininess";
    shininessSlider.html().min = 1.0;
    shininessSlider.html().max = 30;
    shininessSlider.html().step = 0.1;
    shininessSlider.html().value = 1.0;
    shininessSlider.html().onchange = function(e){
        var shine = e.target.value;
        cancer.Graphics.Set.shininess(shine);
    }
    shininessSlider.AppendTo(Win2.elements.content.html());
    
    DockWindow(Win2);
}
BuildInstructionsWindow = function(){
    Win2 = os.windows.WindowsManager.Create.Window("Matrix Multiplication", "PC");
    //Win2.elements.content.html().style.overflow = "hidden";
    Win2.Set.width(1252);
    Win2.Set.height(155);
    Win2.Set.position(0,0);
    Win2.Set.statusbarText("");
    Win2.Hide.menubar();
    Win2.Hide.buttons();
    Win2.Display.window();
    Win2.elements.titlebar.buttons.close.html().onclick = function(e){
        DockWindow(os.windows.WindowsManager.Windows.get(e.target.id));    
    }
    
    UpdateInstructionWindow();
    //DockWindow(Win2);
}
UpdateInstructionWindow = function(){
    
    //Translate
    Win2.elements.content.html().innerHTML ="<table>" +
        "<tr><th colspan='4' rowspan='1'>Translate</th></tr>" +
        "<tr>" +
            "<td>1</td><td>0</td><td>0</td><td>"+ starhawk.Position[0] +"</td>" +
        "</tr>" +
        "<tr>" +
            "<td>0</td><td>1</td><td>0</td><td>"+ starhawk.Position[1] +"</td>" +
        "</tr>" +
        "<tr>" +
            "<td>0</td><td>0</td><td>1</td><td>"+ starhawk.Position[2] +"</td>" +
        "</tr>" +
        "<tr>" +
            "<td>0</td><td>0</td><td>0</td><td>1 </td>" +
        "</tr>" +
    "</table>";
    
    //Scale
    Win2.elements.content.html().innerHTML +="<table>" +
        "<th colspan='4' rowspan='1'>Scale</th>" +
        "<tr>" +
            "<td>" + scX + "</td><td>0</td><td>0</td><td>0</td>" +
        "</tr>" +
        "<tr>" +
            "<td>0</td><td>" + scY + "</td><td>0</td><td>0</td>" +
        "</tr>" +
        "<tr>" +
            "<td>0</td><td>0</td><td>" + scZ + "</td><td>1</td>" +
        "</tr>" +
        "<tr>" +
            "<td>0</td><td>0</td><td>0</td><td>1 </td>" +
        "</tr>" +
    "</table>";
    
    //Rotate Y
    Win2.elements.content.html().innerHTML +="<table>" +
        "<th colspan='4' rowspan='1'>Rotate Y (Yaw)</th>" +
        "<tr>" +
            "<td>cos("+ starhawk.yaw +")</td><td>0</td><td>sin("+ starhawk.yaw  +")</td><td>0</td>" +
        "</tr>" +
        "<tr>" +
            "<td>0</td><td>1</td><td>0</td><td>0</td>" +
        "</tr>" +
        "<tr>" +
            "<td>-sin("+ starhawk.yaw  +")</td><td>0</td><td>cos("+  starhawk.yaw +")</td><td>1</td>" +
        "</tr>" +
        "<tr>" +
            "<td>0</td><td>0</td><td>0</td><td>1 </td>" +
        "</tr>" +
    "</table>";
    
    //Rotate X
    
    Win2.elements.content.html().innerHTML +="<table>" +
        "<th colspan='4' rowspan='1'>Rotate X (Pitch)</th>" +
        "<tr>" +
            "<td>1</td><td>0</td><td>0</td><td>0</td>" +
        "</tr>" +
        "<tr>" +
            "<td>0</td><td>cos("+starhawk.pitch+")</td><td>-sin("+ starhawk.pitch+")</td><td>0</td>" +
        "</tr>" +
        "<tr>" +
            "<td>0</td><td>sin("+starhawk.pitch +")</td><td>cos("+starhawk.pitch +")</td><td>1</td>" +
        "</tr>" +
        "<tr>" +
            "<td>0</td><td>0</td><td>0</td><td>1 </td>" +
        "</tr>" +
    "</table>";
    
    //Rotate Z
    Win2.elements.content.html().innerHTML +="<table>" +
        "<th colspan='4' rowspan='1'>Rotate Z (Roll)</th>" +
        "<tr>" +
            "<td>cos("+starhawk.roll +")</td><td>-sin("+ starhawk.roll +")</td><td>0</td><td>0</td>" +
        "</tr>" +
        "<tr>" +
            "<td>sin("+ starhawk.roll +")</td><td>cos("+ starhawk.roll +")</td><td>0</td><td>0</td>" +
        "</tr>" +
        "<tr>" +
            "<td>0</td><td>0</td><td>1</td><td>1</td>" +
        "</tr>" +
        "<tr>" +
            "<td>0</td><td>0</td><td>0</td><td>1 </td>" +
        "</tr>" +
    "</table></div>";
}
DockWindow = function(win){

    win.Set.position(0,0);
    win.Set.width(358);
    win.Set.height(228);
    win.Hide.menubar();
    win.Hide.titlebar();
    win.Hide.statusbar();
    win.elements.window.Class.Remove("pcWindow");
    win.elements.window.Class.Add("pcWindow-NoShadow");
    document.body.removeChild(win.elements.window.html());
    optionsDiv.appendChild(win.elements.window.html());
    
    var idString = win.Get.id();
    idString = idString.split(".");
    
    var opt = document.createElement("option");
    opt.value = idString[idString.length - 1];
    opt.innerHTML = win.Get.title();
    
    dropDown.add(opt);
    
    dropDown.selectedIndex = opt.index;
    
}

UnDockWindow = function(e){
    var win = win = os.windows.WindowsManager.Windows.get("jahova.window.id." + dropDown[dropDown.selectedIndex].value);
    
    win.Set.position(100,100);
    win.Set.width(358);
    win.Set.height(228);
    win.Display.titlebar();
    win.Display.statusbar();
    win.elements.window.html().style.position = "absolute";
    win.elements.window.Class.Remove("pcWindow-NoShadow");
    win.elements.window.Class.Add("pcWindow");
    win.MakeActive();
    
    optionsDiv.removeChild(win.elements.window.html());
    document.body.appendChild(win.elements.window.html());

    dropDown.remove(dropDown[dropDown.selectedIndex]);
    
    win = os.windows.WindowsManager.Windows.get("jahova.window.id." + dropDown[dropDown.selectedIndex].value);
    win.MakeActive();
    
}